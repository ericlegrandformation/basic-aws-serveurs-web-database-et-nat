resource "aws_route_table" "eu-west-3a-public" {
  vpc_id = aws_vpc.default.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.default.id
  }

  tags = {
    Name = "VPC Public Subnet"
  }
}

resource "aws_route_table_association" "eu-west-3a-public" {
  subnet_id      = aws_subnet.eu-west-3a-public.id
  route_table_id = aws_route_table.eu-west-3a-public.id
}



resource "aws_route_table" "eu-west-3a-private" {
  vpc_id = aws_vpc.default.id

  route {
    cidr_block  = "0.0.0.0/0"
    instance_id = aws_instance.nat.id
  }

  tags = {
    Name = "VPC Private Subnet"
  }
}

resource "aws_route_table_association" "eu-west-3a-private" {
  subnet_id = aws_subnet.eu-west-3a-private.id
  route_table_id = aws_route_table.eu-west-3a-private.id
}

