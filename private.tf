resource "aws_subnet" "eu-west-3a-private" {
  vpc_id = aws_vpc.default.id

  cidr_block        = var.private_subnet_cidr
  availability_zone = "eu-west-3a"

  tags = {
    Name = "VPC Private Subnet"
  }
}

