resource "aws_subnet" "eu-west-3a-public" {
  vpc_id = aws_vpc.default.id

  cidr_block        = var.public_subnet_cidr
  availability_zone = "eu-west-3a"

  tags = {
    Name = "VPC Public Subnet"
  }
}

