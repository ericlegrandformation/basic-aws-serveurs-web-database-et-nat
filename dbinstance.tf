
resource "aws_instance" "db-1" {
  ami                    = var.amis[var.region]
  availability_zone      = "eu-west-3a"
  instance_type          = "t2.micro"
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.db.id]
  subnet_id              = aws_subnet.eu-west-3a-private.id
  
  source_dest_check = false

  tags = {
    Name = "Database Server"
  }
}
